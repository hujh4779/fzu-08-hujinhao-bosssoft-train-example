package com.bosssoft.hr.train.j2se.basic.example.annotation;

import com.bosssoft.hr.train.j2se.basic.example.database.DBUtil;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:41
 * @since
 **/
public abstract class BaseModel {

    public int save() {
        Class userModel = this.getClass();
        StringBuilder front = new StringBuilder();
        StringBuilder back = new StringBuilder();
        front.append("INSERT INTO ");
        back.append(") VALUES (");
        if (userModel != null) {
            if (userModel.isAnnotationPresent(Table.class)) {
                Field[] fields = userModel.getDeclaredFields();
                if (fields.length == 0) {
                    return 0;
                }
                Table table = (Table) userModel.getAnnotation(Table.class);
                front.append(table.value()).append(" (");
                boolean flag = false;
                try {
                    for (Field field : fields) {
                        field.setAccessible(true);
                        if (field.isAnnotationPresent(Id.class)) {
                            Id id = field.getAnnotation(Id.class);
                            flag = true;
                            front.append(id.value()).append(",");
                            back.append(field.get(this)).append(",");
                        } else if (field.isAnnotationPresent(Column.class)) {
                            Column column = field.getAnnotation(Column.class);
                            front.append(column.value()).append(",");
                            if (field.get(this) instanceof String) {
                                back.append("'").append(field.get(this)).append("',");
                            } else {
                                back.append(field.get(this)).append(",");
                            }
                        }
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                if (!flag) {
                    System.out.println("无@Id注解");
                    return 0;
                }
                String sqlString = front.substring(0, front.length() - 1)+ back.substring(0, back.length() - 1) + ")";
                DBUtil.init();
                int i = DBUtil.executeUpdate(sqlString);
                DBUtil.closeConnection();
                return i;
            } else {
                System.out.println("无@Table注解");
            }
        }
        return 0;
    }

    public int update() {
        Class userModel = this.getClass();
        StringBuilder front = new StringBuilder();
        StringBuilder back = new StringBuilder();
        front.append("UPDATE ");
        back.append(" WHERE ");
        if (userModel != null) {
            if (userModel.isAnnotationPresent(Table.class)) {
                Field[] fields = userModel.getDeclaredFields();
                if (fields.length == 0) {
                    return 0;
                }
                Table table = (Table) userModel.getAnnotation(Table.class);
                front.append(table.value()).append(" SET ");
                boolean flag = false;
                try {
                    for (Field field : fields) {
                        field.setAccessible(true);
                        if (field.isAnnotationPresent(Id.class)) {
                            Id id = field.getAnnotation(Id.class);
                            flag = true;
                            back.append(id.value()).append("=").append(field.get(this));
                        } else if (field.isAnnotationPresent(Column.class)) {
                            Column column = field.getAnnotation(Column.class);
                            if (field.get(this) instanceof String) {
                                front.append(column.value()).append("='").append(field.get(this)).append("',");
                            } else {
                                front.append(column.value()).append("=").append(field.get(this)).append(",");
                            }
                        }
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                if (!flag) {
                    System.out.println("无@Id注解");
                    return 0;
                }
                String sqlString = front.substring(0, front.length() - 1) + back;
                DBUtil.init();
                int i = DBUtil.executeUpdate(sqlString);
                DBUtil.closeConnection();
                return i;
            } else {
                System.out.println("无@Table注解");
            }
        }
        return 0;
    }

    public int remove() {
        Class userModel = this.getClass();
        StringBuilder front = new StringBuilder();
        StringBuilder back = new StringBuilder();
        front.append("DELETE FROM ");
        back.append(" WHERE ");
        if (userModel != null) {
            if (userModel.isAnnotationPresent(Table.class)) {
                Field[] fields = userModel.getDeclaredFields();
                if (fields.length == 0) {
                    return 0;
                }
                Table table = (Table) userModel.getAnnotation(Table.class);
                front.append(table.value());
                boolean flag = false;
                try {
                    for (Field field : fields) {
                        field.setAccessible(true);
                        if (field.isAnnotationPresent(Id.class)) {
                            Id id = field.getAnnotation(Id.class);
                            flag = true;
                            back.append(id.value()).append("=").append(field.get(this));
                        }
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                if (!flag) {
                    System.out.println("无@Id注解");
                    return 0;
                }
                String sqlString = front.toString() + back.toString();
                DBUtil.init();
                int i = DBUtil.executeUpdate(sqlString);
                DBUtil.closeConnection();
                return i;
            } else {
                System.out.println("无@Table注解");
            }
        }
        return 0;
    }

    public List<Object> queryForList() {
        Class userModel = this.getClass();
        List<Object> userModelList = new ArrayList<>();
        StringBuilder front = new StringBuilder();
        front.append("SELECT * FROM ");
        if (userModel != null) {
            if (userModel.isAnnotationPresent(Table.class)) {
                Field[] fields = userModel.getDeclaredFields();
                if (fields.length == 0) {
                    return userModelList;
                }
                Table table = (Table) userModel.getAnnotation(Table.class);
                front.append(table.value());
                String sqlString = front.toString();
                Map<String, String> map = new HashMap<>();
                for (Field field : fields) {
                    field.setAccessible(true);
                    if (field.isAnnotationPresent(Id.class)) {
                        Id id = field.getAnnotation(Id.class);
                        map.put(id.value(), field.getName());
                    } else if (field.isAnnotationPresent(Column.class)) {
                        Column column = field.getAnnotation(Column.class);
                        map.put(column.value(), field.getName());
                    }
                }
                DBUtil.init();
                ResultSet resultSet = DBUtil.executeQuery(sqlString);
                try {
                    int columnCount = resultSet.getMetaData().getColumnCount();
                    while (resultSet.next()) {
                        BaseModel instance = this.getClass().newInstance();
                        for (int i = 1; i <= columnCount; i++) {
                            Field field = instance.getClass().getDeclaredField(map.get(resultSet.getMetaData().getColumnName(i)));
                            field.setAccessible(true);
                            field.set(instance, resultSet.getObject(i));
                        }
                        userModelList.add(instance);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    DBUtil.closeConnection();
                }
            } else {
                System.out.println("无@Table注解");
            }
        }
        return userModelList;
    }
}
