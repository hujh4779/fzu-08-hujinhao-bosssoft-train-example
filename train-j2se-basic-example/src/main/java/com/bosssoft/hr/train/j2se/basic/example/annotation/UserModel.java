package com.bosssoft.hr.train.j2se.basic.example.annotation;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:42
 * @since
 **/
@Table(value = "t_user")
public class UserModel extends BaseModel {

    @Id(value = "id")
    private Long ID;

    @Column(value = "name")
    private String name;

    @Column(value = "age")
    private Integer age;

    public Long getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "{" + "Id=" + ID + ", name='" + name + '\'' + ", age=" + age + '}';
    }
}
