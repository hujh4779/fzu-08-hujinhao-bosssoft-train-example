package com.bosssoft.hr.train.j2se.basic.example.database;

import java.sql.*;

/**
 * @description:  我是工具类并且我不喜欢被继承 final 保护了我免于继承，private 保护我被创建
 * @author: Administrator
 * @create: 2020-05-28 20:45
 * @since
 **/
public final class DBUtil {
    private static final String DB_URL = "jdbc:mysql://47.100.50.81:3306/bosssoft_example_j2se?useSSL=false&serverTimezone=Asia/Shanghai";
    private static final String USER = "root";
    private static final String PASSWORD = "aliyun123456";

    private static Connection connection;
    private static PreparedStatement ps;
    private static ResultSet rs;

    private DBUtil(){

    }

    public static void init() {
        connection = createConnection();
        ps = null;
        rs = null;
    }

    private static Connection createConnection(){
        Connection connection = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(DB_URL, USER, PASSWORD);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }

    /**
     *  如果需要参数的你可以选择overload 改方法
     * @param sql
     * @return
     */
    public static ResultSet executeQuery(String sql){
        if (connection != null) {
            try {
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery(sql);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return rs;
    }

    public static int executeUpdate(String sql) {
        if (connection != null) {
            try {
                ps = connection.prepareStatement(sql);
                return ps.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public static void closeConnection() {
        try {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
