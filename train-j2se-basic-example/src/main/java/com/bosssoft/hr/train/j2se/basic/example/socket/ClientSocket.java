package com.bosssoft.hr.train.j2se.basic.example.socket;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * @param
 * @author: Administrator
 * @create: 2020-05-28 22:25
 * @since
 **/
public class ClientSocket implements  Starter{

    private ByteBuffer read = ByteBuffer.allocate(1024);
    private ByteBuffer write = ByteBuffer.allocate(1024);

    @Override
    public boolean start() {
        SocketChannel socketChannel = null;
        try {
            // 生成socketChannel并设置为非阻塞模式
            socketChannel = SocketChannel.open(new InetSocketAddress(8089));
            socketChannel.configureBlocking(false);

            // 写入
            write.clear();
            write.put("Hello Server".getBytes());
            write.flip();
            socketChannel.write(write);
            write.clear();

            // 读取
            int num = 0;
            while ((num = socketChannel.read(read)) != -1) {
                read.flip();
                String s = new String(read.array(), 0, num);
                System.out.println(s);
                read.clear();
            }

            return true;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (socketChannel != null) {
                try {
                    socketChannel.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }
}
