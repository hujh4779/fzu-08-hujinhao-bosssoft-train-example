package com.bosssoft.hr.train.j2se.basic.example.socket;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

/**
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:25
 * @since
 **/
public class NIOServerSocket implements Starter{

    private ByteBuffer read = ByteBuffer.allocate(1024);
    private ByteBuffer write = ByteBuffer.allocate(1024);

    @Override
    public boolean start() {
        try {
            // 生成serverSocketChannel并设置为非阻塞模式
            ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
            serverSocketChannel.configureBlocking(false);
            // 生成并注册selector
            Selector selector = Selector.open();
            serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
            // 绑定接口
            serverSocketChannel.bind(new InetSocketAddress(8089));
            System.out.println("服务器启动");

            while (selector.select() > 0) {
                Set<SelectionKey> keys = selector.selectedKeys();
                Iterator<SelectionKey> iterator = keys.iterator();
                while (iterator.hasNext()) {
                    SelectionKey key = iterator.next();
                    if (key.isAcceptable()) {
                        SocketChannel socketChannel = serverSocketChannel.accept();
                        socketChannel.configureBlocking(false);
                        socketChannel.register(selector, SelectionKey.OP_READ);
                        System.out.println("成功连接");
                    } else if (key.isWritable()) {
                        write.clear();
                        write.put("写入信息".getBytes());
                        write.flip();
                        SocketChannel socketChannel = (SocketChannel) key.channel();
                        socketChannel.configureBlocking(false);
                        socketChannel.write(write);
                        socketChannel.register(selector, SelectionKey.OP_READ);
                    } else if (key.isReadable()) {
                        read.clear();
                        SocketChannel socketChannel = (SocketChannel) key.channel();
                        socketChannel.configureBlocking(false);
                        int num;
                        if ((num = socketChannel.read(read)) == -1) {
                            System.out.println("未读到信息");
                        } else {
                            socketChannel.register(selector, SelectionKey.OP_WRITE);
                            String s = new String(read.array(), 0, num);
                            System.out.println(s);
                        }
                        write.clear();
                        write.put("Hello Client".getBytes());
                        write.flip();
                        socketChannel.write(write);
                        socketChannel.close();
                    }
                    iterator.remove();
                }
            }
            serverSocketChannel.close();
            selector.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
