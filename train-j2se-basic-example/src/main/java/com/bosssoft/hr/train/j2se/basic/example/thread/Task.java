package com.bosssoft.hr.train.j2se.basic.example.thread;

import java.util.Random;

/**
 * @author HS
 */
public class Task implements Runnable {

    private String name;
    private Random random = new Random();

    public Task(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(random.nextInt(1000));
            System.out.println("Task" + name + "完成");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
