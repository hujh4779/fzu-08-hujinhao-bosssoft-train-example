package com.bosssoft.hr.train.j2se.basic.example.thread;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * @author HS
 */
public class ThreadPool {

    private static int WORK_TASK_NUM = 5;
    private static int MAX_TASK_NUM = 10;
    private final int workTaskNum;
    private WorkThread[] workThreads;
    private final BlockingQueue<Runnable> taskQueue;

    public ThreadPool (int workTaskNum, int maxTaskNum) {
        if (workTaskNum <= 0) {
            workTaskNum = WORK_TASK_NUM;
        }
        if (maxTaskNum <= 0) {
            maxTaskNum = MAX_TASK_NUM;
        }
        this.workTaskNum = workTaskNum;
        taskQueue = new ArrayBlockingQueue<>(maxTaskNum);
        workThreads = new WorkThread[workTaskNum];
        for (int i = 0; i < workTaskNum; i++) {
            workThreads[i] = new WorkThread();
            workThreads[i].start();
        }
    }

    private class WorkThread extends Thread {
        @Override
        public void run() {
            Runnable runnable = null;
            try {
                while (!isInterrupted()) {
                    runnable = taskQueue.take();
                    if (runnable != null) {
                        runnable.run();
                    }
                    runnable = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void stopTask() {
            interrupt();
        }
    }

    public void execute(Runnable task) {
        try {
            taskQueue.put(task);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void destroy() {
        for (int i = 0; i < workTaskNum; i++) {
            workThreads[i].stopTask();
            workThreads[i] = null;
        }
        taskQueue.clear();
    }

}
