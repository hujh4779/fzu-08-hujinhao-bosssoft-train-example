package com.bosssoft.hr.train.j2se.basic.example.xml;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Student;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:13
 * @since
 **/
public class DOMOperation implements XMLOperation<Student> {

    private String xmlPath = "src/main/resources/student.tld";
    private static String STUDENT = "student";
    private static String NAME = "name";
    private static String AGE = "age";

    public Document getDocument() {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setIgnoringElementContentWhitespace(true);
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document document = db.parse(xmlPath);
            return document;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean save(Document document) {
        TransformerFactory factory = TransformerFactory.newInstance();
        try {
            Transformer transformer = factory.newTransformer();
            transformer.transform(new DOMSource(document), new StreamResult(new File(xmlPath)));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean create(Student object) {
        Document document = getDocument();
        Element root = document.getDocumentElement();
        Element student = document.createElement(STUDENT);
        student.setAttribute("id", object.getId().toString());
        Element name = document.createElement(NAME);
        name.setTextContent(object.getName());
        Element age = document.createElement(AGE);
        age.setTextContent(object.getAge().toString());
        student.appendChild(name);
        student.appendChild(age);
        root.appendChild(student);
        return save(document);
    }

    @Override
    public boolean remove(Student object) {
        Document document = getDocument();
        NodeList students = document.getElementsByTagName(STUDENT);
        for (int i = 0; i < students.getLength(); i++) {
            Element student = (Element)students.item(i);
            if (object.getId().equals(Integer.parseInt(student.getAttribute("id")))) {
                student.getParentNode().removeChild(student);
                return save(document);
            }
        }
        return false;
    }

    @Override
    public boolean update(Student object) {
        Document document = getDocument();
        NodeList students = document.getElementsByTagName(STUDENT);
        for (int i = 0; i < students.getLength(); i++) {
            Element student = (Element)students.item(i);
            if (object.getId().equals(Integer.parseInt(student.getAttribute("id")))) {
                student.getElementsByTagName(NAME).item(0).setTextContent(object.getName());
                student.getElementsByTagName(AGE).item(0).setTextContent(object.getAge().toString());
                return save(document);
            }
        }
        return false;
    }

    @Override
    public Student query(Student object) {
        Document document = getDocument();
        NodeList students = document.getElementsByTagName(STUDENT);
        for (int i = 0; i < students.getLength(); i++) {
            Element student = (Element)students.item(i);
            if (object.getId().equals(Integer.parseInt(student.getAttribute("id")))) {
                object.setName(student.getElementsByTagName(NAME).item(0).getTextContent());
                object.setAge(Integer.parseInt(student.getElementsByTagName(AGE).item(0).getTextContent()));
                return object;
            }
        }
        return null;
    }
}
