package com.bosssoft.hr.train.j2se.basic.example.xml;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Student;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:10
 * @since
 **/
public class SAXOperatiron implements XMLOperation<Student> {

    private String xmlPath = "src/main/resources/student.tld";
    private static String STUDENT = "student";
    private static String NAME = "name";
    private static String AGE = "age";

    public Document getDocument() {
        SAXReader saxReader = new SAXReader();
        try {
            Document document = saxReader.read(new File(xmlPath));
            return document;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean save(Document document) {
        OutputFormat outputFormat = OutputFormat.createPrettyPrint();
        try {
            XMLWriter xmlWriter = new XMLWriter(new FileOutputStream(new File(xmlPath)), outputFormat);
            xmlWriter.write(document);
            xmlWriter.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean create(Student object) {
        Document document = getDocument();
        Element root = document.getRootElement();
        Element student = root.addElement(STUDENT);
        student.addAttribute("id", object.getId().toString());
        Element name = student.addElement(NAME);
        name.setText(object.getName());
        Element age = student.addElement(AGE);
        age.setText(object.getAge().toString());
        return save(document);
    }

    @Override
    public boolean remove(Student object) {
        Document document = getDocument();
        Element root = document.getRootElement();
        Iterator<Element> iterator = root.elementIterator(STUDENT);
        while (iterator.hasNext()) {
            Element student = iterator.next();
            if (object.getId().equals(Integer.parseInt(student.attributeValue("id")))) {
                iterator.remove();
                return save(document);
            }
        }
        return false;
    }

    @Override
    public boolean update(Student object) {
        Document document = getDocument();
        Element root = document.getRootElement();
        List<Element> students = root.elements(STUDENT);
        for (Element student : students) {
            if (object.getId().equals(Integer.parseInt(student.attributeValue("id")))) {
                student.element("name").setText(object.getName());
                student.element("age").setText(object.getAge().toString());
                return save(document);
            }
        }
        return false;
    }

    @Override
    public Student query(Student object) {
        Document document = getDocument();
        Element root = document.getRootElement();
        List<Element> students = root.elements(STUDENT);
        for (Element student : students) {
            if (object.getId().equals(Integer.parseInt(student.attributeValue("id")))) {
                object.setName(student.elementText(NAME));
                object.setAge(Integer.parseInt(student.elementText(AGE)));
                return object;
            }
        }
        return null;
    }
}
