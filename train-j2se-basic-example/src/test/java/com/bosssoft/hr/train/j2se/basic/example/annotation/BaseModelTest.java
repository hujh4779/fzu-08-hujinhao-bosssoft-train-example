package com.bosssoft.hr.train.j2se.basic.example.annotation;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BaseModelTest {

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void save() {
        UserModel userModel = new UserModel();
        userModel.setID(1L);
        userModel.setName("userModel1");
        userModel.setAge(20);
        assertEquals(1, userModel.save());
    }

    @Test
    public void update() {
        UserModel userModel = new UserModel();
        userModel.setID(2L);
        userModel.setName("um2");
        userModel.setAge(22);
        assertEquals(1, userModel.update());
    }

    @Test
    public void remove() {
        UserModel userModel = new UserModel();
        userModel.setID(3L);
        assertEquals(1, userModel.remove());
    }

    @Test
    public void queryForList() {
        UserModel userModel = new UserModel();
        System.out.println(userModel.queryForList());
    }
}