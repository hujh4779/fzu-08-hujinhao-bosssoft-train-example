package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArrayListExampleImplTest {

    ArrayListExampleImpl arrayListExample;
    User user1;
    User user2;
    User user3;

    @Before
    public void setUp() throws Exception {
        arrayListExample = new ArrayListExampleImpl();
        user1 = new User(1, "user1");
        user2 = new User(2, "user2");
        user3 = new User(3, "user3");
    }

    @After
    public void tearDown() throws Exception {
        arrayListExample = null;
        user1 = null;
        user2 = null;
        user3 = null;
    }

    @Test
    public void append() {
        // 向arraylist中添加user1
        assertTrue(arrayListExample.append(user1));
        // 向arraylist中添加user2
        assertTrue(arrayListExample.append(user2));
    }

    @Test
    public void get() {
        assertTrue(arrayListExample.append(user1));
        // 下标正常
        assertEquals(user1, arrayListExample.get(0));
        // 下标负数
        assertNull(arrayListExample.get(-1));
        // 下标越界
        assertNull(arrayListExample.get(1));
    }

    @Test
    public void insert() {
        assertTrue(arrayListExample.append(user1));
        // 插入位置正常
        assertTrue(arrayListExample.insert(0, user2));
        // 插入位置负数
        assertFalse(arrayListExample.insert(-1, user2));
        // 插入位置越界
        assertFalse(arrayListExample.insert(2, user2));
        // 插入位置为末尾时报错
        //assertFalse(arrayListExample.insert(1, user2));
    }

    @Test
    public void remove() {
        assertTrue(arrayListExample.append(user1));
        assertTrue(arrayListExample.append(user2));
        // 下标正常
        assertTrue(arrayListExample.remove(0));
        // 下标负数
        assertFalse(arrayListExample.remove(-1));
        // 下标越界
        assertFalse(arrayListExample.remove(2));
    }

    @Test
    public void listByIndex() {
        assertTrue(arrayListExample.append(user1));
        assertTrue(arrayListExample.append(user2));
        // 测试listByIndex
        arrayListExample.listByIndex();
    }

    @Test
    public void listByIterator() {
        assertTrue(arrayListExample.append(user1));
        assertTrue(arrayListExample.append(user2));
        // 测试listByIterator
        arrayListExample.listByIterator();
    }

    @Test
    public void toArray() {
        assertTrue(arrayListExample.append(user3));
        assertTrue(arrayListExample.append(user1));
        assertTrue(arrayListExample.append(user2));
        // 测试toArray
        User[] users = new User[]{user3, user1, user2};
        assertArrayEquals(users, arrayListExample.toArray());
    }

    @Test
    public void sort() {
        assertTrue(arrayListExample.append(user3));
        assertTrue(arrayListExample.append(user1));
        assertTrue(arrayListExample.append(user2));
        // 测试sort
        arrayListExample.sort();
        assertEquals(user1, arrayListExample.get(0));
        assertEquals(user2, arrayListExample.get(1));
        assertEquals(user3, arrayListExample.get(2));
    }

    @Test
    public void sort2() {
        assertTrue(arrayListExample.append(user3));
        assertTrue(arrayListExample.append(user1));
        assertTrue(arrayListExample.append(user2));
        // 测试sort2
        arrayListExample.sort2();
        assertEquals(user1, arrayListExample.get(0));
        assertEquals(user2, arrayListExample.get(1));
        assertEquals(user3, arrayListExample.get(2));
    }
}