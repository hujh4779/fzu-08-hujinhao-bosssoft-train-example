package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Resource;
import com.bosssoft.hr.train.j2se.basic.example.pojo.Role;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ConcurrentHashMapExampleImplTest {

    ConcurrentHashMapExampleImpl concurrentHashMapExample;
    Role role1;
    Role role2;
    Resource resource1;
    Resource resource2;

    @Before
    public void setUp() throws Exception {
        concurrentHashMapExample = new ConcurrentHashMapExampleImpl();
        role1 = new Role(1, "administrator");
        role2 = new Role(2, "user");
        resource1 = new Resource(1, "menu");
        resource2 = new Resource(2, "button");
    }

    @After
    public void tearDown() throws Exception {
        concurrentHashMapExample = null;
        role1 = null;
        role2 = null;
        resource1 = null;
        resource2 = null;
    }

    @Test
    public void put() {
        // 键不可为null
        //assertNull(concurrentHashMapExample.put(null, null));
        // 测试放入正常值
        assertNull(concurrentHashMapExample.put(role1, resource1));
    }

    @Test
    public void remove() {
        assertNull(concurrentHashMapExample.put(role1, resource1));
        // 测试正常删除
        assertEquals(resource1, concurrentHashMapExample.remove(role1));
        // 测试无键
        assertNull(concurrentHashMapExample.remove(role2));
    }

    @Test
    public void containsKey() {
        assertNull(concurrentHashMapExample.put(role1, resource1));
        // 测试有键
        assertTrue(concurrentHashMapExample.containsKey(role1));
        // 测试无键
        assertFalse(concurrentHashMapExample.containsKey(role2));
    }

    @Test
    public void visitByEntryset() {
        assertNull(concurrentHashMapExample.put(role1, resource1));
        assertNull(concurrentHashMapExample.put(role2, resource2));
        // 测试visitByEntryset
        concurrentHashMapExample.visitByEntryset();
    }

    @Test
    public void visitByEntryset_Lambda() {
        assertNull(concurrentHashMapExample.put(role1, resource1));
        assertNull(concurrentHashMapExample.put(role2, resource2));
        // 测试visitByEntryset_Lambda
        concurrentHashMapExample.visitByEntryset_Lambda();
    }

    @Test
    public void visitByKeyset() {
        assertNull(concurrentHashMapExample.put(role1, resource1));
        assertNull(concurrentHashMapExample.put(role2, resource2));
        // 测试visitByKeyset
        concurrentHashMapExample.visitByKeyset();
    }

    @Test
    public void visitByValues() {
        assertNull(concurrentHashMapExample.put(role1, resource1));
        assertNull(concurrentHashMapExample.put(role2, resource2));
        // 测试visitByValues
        concurrentHashMapExample.visitByValues();
    }
}