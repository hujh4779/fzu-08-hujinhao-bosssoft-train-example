package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Resource;
import com.bosssoft.hr.train.j2se.basic.example.pojo.Role;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class HashMapExampleImplTest {
    HashMapExampleImpl hashMapExample;
    Role role1;
    Role role2;
    Resource resource1;
    Resource resource2;

    @Before
    public void setUp() throws Exception {
        hashMapExample = new HashMapExampleImpl();
        role1 = new Role(1, "administrator");
        role2 = new Role(2, "user");
        resource1 = new Resource(1, "menu");
        resource2 = new Resource(2, "button");
    }

    @After
    public void tearDown() throws Exception {
        hashMapExample = null;
        role1 = null;
        role2 = null;
        resource1 = null;
        resource2 = null;
    }

    @Test
    public void put() {
        // 测试null键null值
        assertNull(hashMapExample.put(null, null));
        // 测试放入正常值
        assertNull(hashMapExample.put(role1, resource1));
    }

    @Test
    public void remove() {
        assertNull(hashMapExample.put(role1, resource1));
        // 测试正常删除
        assertEquals(resource1, hashMapExample.remove(role1));
        // 测试无键
        assertNull(hashMapExample.remove(role2));
    }

    @Test
    public void containsKey() {
        assertNull(hashMapExample.put(role1, resource1));
        // 测试有键
        assertTrue(hashMapExample.containsKey(role1));
        // 测试无键
        assertFalse(hashMapExample.containsKey(role2));
    }

    @Test
    public void visitByEntryset() {
        assertNull(hashMapExample.put(role1, resource1));
        assertNull(hashMapExample.put(role2, resource2));
        // 测试visitByEntryset
        hashMapExample.visitByEntryset();
    }

    @Test
    public void visitByKeyset() {
        assertNull(hashMapExample.put(role1, resource1));
        assertNull(hashMapExample.put(role2, resource2));
        // 测试visitByKeyset
        hashMapExample.visitByKeyset();
    }

    @Test
    public void visitByValues() {
        assertNull(hashMapExample.put(role1, resource1));
        assertNull(hashMapExample.put(role2, resource2));
        // 测试visitByValues
        hashMapExample.visitByValues();
    }
}