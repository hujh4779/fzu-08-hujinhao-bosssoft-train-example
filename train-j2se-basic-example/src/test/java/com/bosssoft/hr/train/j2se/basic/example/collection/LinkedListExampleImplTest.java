package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LinkedListExampleImplTest {

    LinkedListExampleImpl linkedListExample;
    User user1;
    User user2;
    User user3;

    @Before
    public void setUp() throws Exception {
        linkedListExample = new LinkedListExampleImpl();
        user1 = new User(1, "user1");
        user2 = new User(2, "user2");
        user3 = new User(3, "user3");
    }

    @After
    public void tearDown() throws Exception {
        linkedListExample = null;
        user1 = null;
        user2 = null;
        user3 = null;
    }

    @Test
    public void append() {
        // 向linkedList中添加user1
        assertEquals(true, linkedListExample.append(user1));
        // 向linkedList中添加user2
        assertEquals(true, linkedListExample.append(user2));
    }

    @Test
    public void get() {
        assertEquals(true, linkedListExample.append(user1));
        // 下标正常
        assertEquals(user1, linkedListExample.get(0));
        // 下标负数和越界
        assertNull(linkedListExample.get(-1));
        assertNull(linkedListExample.get(2));
    }

    @Test
    public void insert() {
        assertTrue(linkedListExample.append(user1));
        // 插入位置正常
        assertTrue(linkedListExample.insert(0, user2));
        // 插入位置负数和越界
        assertFalse(linkedListExample.insert(-1, user2));
        assertFalse(linkedListExample.insert(2, user2));
    }

    @Test
    public void remove() {
        assertTrue(linkedListExample.append(user1));
        assertTrue(linkedListExample.append(user2));
        // 下标正常
        assertTrue(linkedListExample.remove(0));
        // 下标负数和越界
        assertFalse(linkedListExample.remove(-1));
        assertFalse(linkedListExample.remove(2));
    }

    @Test
    public void listByIndex() {
        assertTrue(linkedListExample.append(user1));
        assertTrue(linkedListExample.append(user2));
        // 测试listByIndex
        linkedListExample.listByIndex();
    }

    @Test
    public void listByIterator() {
        assertTrue(linkedListExample.append(user1));
        assertTrue(linkedListExample.append(user2));
        // 测试listByIterator
        linkedListExample.listByIterator();
    }

    @Test
    public void toArray() {
        assertTrue(linkedListExample.append(user3));
        assertTrue(linkedListExample.append(user1));
        assertTrue(linkedListExample.append(user2));
        // 测试toArray
        User[] users = new User[]{user3, user1, user2};
        assertArrayEquals(users, linkedListExample.toArray());
    }

    @Test
    public void sort() {
        assertTrue(linkedListExample.append(user3));
        assertTrue(linkedListExample.append(user1));
        assertTrue(linkedListExample.append(user2));
        // 测试sort
        linkedListExample.sort();
        assertEquals(user1, linkedListExample.get(0));
        assertEquals(user2, linkedListExample.get(1));
        assertEquals(user3, linkedListExample.get(2));
    }

    @Test
    public void sort2() {
        assertTrue(linkedListExample.append(user3));
        assertTrue(linkedListExample.append(user1));
        assertTrue(linkedListExample.append(user2));
        // 测试sort2
        linkedListExample.sort2();
        assertEquals(user1, linkedListExample.get(0));
        assertEquals(user2, linkedListExample.get(1));
        assertEquals(user3, linkedListExample.get(2));
    }

    @Test
    public void addFirst() {
        assertTrue(linkedListExample.append(user2));
        // 测试addFirst
        linkedListExample.addFirst(user1);
        assertEquals(user1, linkedListExample.get(0));
    }

    @Test
    public void offer() {
        assertTrue(linkedListExample.offer(user1));
        assertTrue(linkedListExample.offer(user2));
    }

    @Test
    public void sychronizedVisit() {
        linkedListExample.sychronizedVisit(user1);
        assertEquals(user1, linkedListExample.get(0));
    }

    @Test
    public void push() {
        linkedListExample.push(user1);
        linkedListExample.push(user2);
        assertEquals(user1, linkedListExample.get(1));
    }

    @Test
    public void pop() {
        linkedListExample.push(user1);
        linkedListExample.push(user2);
        assertEquals(user2, linkedListExample.pop());
    }
}