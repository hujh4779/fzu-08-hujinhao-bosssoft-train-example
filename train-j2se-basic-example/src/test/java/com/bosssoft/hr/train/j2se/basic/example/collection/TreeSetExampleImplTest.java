package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TreeSetExampleImplTest {

    TreeSetExampleImpl treeSetExample;
    User user1;
    User user2;
    User user3;

    @Before
    public void setUp() throws Exception {
        treeSetExample = new TreeSetExampleImpl();
        user1 = new User(1, "user1");
        user2 = new User(2, "user2");
        user3 = new User(3, "user3");
    }

    @After
    public void tearDown() throws Exception {
        treeSetExample = null;
        user1 = null;
        user2 = null;
        user3 = null;
    }

    @Test
    public void sort() {
        User[] users = new User[]{user3, user1, user2};
        User[] sortedUsers = treeSetExample.sort(users);
        // 测试排序是否成功
        assertEquals(user1, sortedUsers[0]);
        assertEquals(user2, sortedUsers[1]);
        assertEquals(user3, sortedUsers[2]);
    }
}