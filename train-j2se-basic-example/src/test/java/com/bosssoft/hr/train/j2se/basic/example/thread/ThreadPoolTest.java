package com.bosssoft.hr.train.j2se.basic.example.thread;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ThreadPoolTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void threadPoolTest() throws InterruptedException {
        ThreadPool threadPool = new ThreadPool(2, 4);
        threadPool.execute(new Task("1"));
        threadPool.execute(new Task("2"));
        threadPool.execute(new Task("3"));
        threadPool.execute(new Task("4"));
        threadPool.execute(new Task("5"));
        Thread.sleep(5000);
        threadPool.destroy();
    }
}