package com.bosssoft.hr.train.j2se.basic.example.xml;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Student;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DOMOperationTest {

    DOMOperation domOperation;
    Student student1;
    Student student2;

    @Before
    public void setUp() throws Exception {
        domOperation = new DOMOperation();
        student1 = new Student(1, "student1", 20);
        student2 = new Student(2, "student2", 22);
    }

    @After
    public void tearDown() throws Exception {
        domOperation = null;
        student1 = null;
        student2 = null;
    }

    @Test
    public void create() {
        assertTrue(domOperation.create(student1));
        assertTrue(domOperation.create(student2));
    }

    @Test
    public void remove() {
        assertTrue(domOperation.remove(student2));
    }

    @Test
    public void update() {
        student1.setName("updatedStudent1");
        student1.setAge(24);
        assertTrue(domOperation.update(student1));
    }

    @Test
    public void query() {
        student1.setName("updatedStudent1");
        student1.setAge(24);
        assertEquals(student1, domOperation.query(student1));
    }
}