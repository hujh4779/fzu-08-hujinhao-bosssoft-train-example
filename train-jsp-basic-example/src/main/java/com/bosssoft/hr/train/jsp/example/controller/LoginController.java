package com.bosssoft.hr.train.jsp.example.controller;

import com.bosssoft.hr.train.jsp.example.pojo.User;
import com.bosssoft.hr.train.jsp.example.service.UserService;
import com.bosssoft.hr.train.jsp.example.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-30 10:11
 * @since
 **/

public class LoginController extends HttpServlet {

    UserService userService = new UserServiceImpl();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // 校验参数合法性如果没问题才调用
        User user=new User();
        if (req.getParameter("code")!=null && !req.getParameter("code").equals("")) {
            user.setId(null);
            user.setName(null);
            user.setCode(req.getParameter("code"));
            user.setPassword(req.getParameter("password"));
                if(authentication(user)) {
                    HttpSession session = req.getSession();
                    session.setAttribute("code", user.getCode());
                    resp.getWriter().print(true);
                } else {
                    resp.getWriter().print(false);
                }
        }else{
            // 错误应答或者跳转错误页面
            resp.getWriter().print(false);
        }
    }

    /**
     *  public 后面改为 private
     * @param user
     * @return
     */
    private boolean authentication(User user){
        return userService.authentication(user);
    }
}
