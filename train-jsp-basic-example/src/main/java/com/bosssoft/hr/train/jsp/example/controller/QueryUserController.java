package com.bosssoft.hr.train.jsp.example.controller;

import com.bosssoft.hr.train.jsp.example.pojo.Query;
import com.bosssoft.hr.train.jsp.example.pojo.User;
import com.bosssoft.hr.train.jsp.example.service.UserService;
import com.bosssoft.hr.train.jsp.example.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @description:
 * @author: Administrator
 * @create: 2020-05-30 11:22
 * @since
 **/
public class QueryUserController extends HttpServlet {
    /**
     *  用户对象
     */
    private UserService userService=new UserServiceImpl();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //校验参数合法性如果没问题才调用
        Query query = new Query(req.getParameter("name"));

        try{
            List<User> userList = queryUser(query);
            req.setAttribute("userList", userList);
            req.getRequestDispatcher("index.jsp").forward(req, resp);
        }catch (Exception ex) {
            req.getRequestDispatcher("err.jsp").forward(req, resp);
        }
    }

    private List<User> queryUser(Query query){
        return  userService.queryByCondition(query);
    }
}
