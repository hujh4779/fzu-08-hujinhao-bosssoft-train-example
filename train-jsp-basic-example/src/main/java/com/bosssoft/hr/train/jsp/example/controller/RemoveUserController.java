package com.bosssoft.hr.train.jsp.example.controller;

import com.bosssoft.hr.train.jsp.example.pojo.Query;
import com.bosssoft.hr.train.jsp.example.pojo.User;
import com.bosssoft.hr.train.jsp.example.service.UserService;
import com.bosssoft.hr.train.jsp.example.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-30 11:22
 * @since
 **/
public class RemoveUserController extends HttpServlet {
    /**
     *  用户对象
     */
    private UserService userService=new UserServiceImpl();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        User user = new User();
        user.setId(Integer.parseInt(req.getParameter("id")));

        try{
            if (removeUser(user)) {
                resp.getWriter().print(true);
                req.getRequestDispatcher("queryUser").forward(req, resp);
            } else {
                resp.getWriter().print(true);
                req.getRequestDispatcher("err.jsp").forward(req, resp);
            }
        } catch (Exception ex){
            req.getRequestDispatcher("err.jsp").forward(req, resp);
        }
    }

    private boolean removeUser(User user){
        return userService.remove(user);
    }
}
