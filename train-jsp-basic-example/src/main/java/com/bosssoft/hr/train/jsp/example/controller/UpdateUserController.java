package com.bosssoft.hr.train.jsp.example.controller;

import com.bosssoft.hr.train.jsp.example.pojo.Query;
import com.bosssoft.hr.train.jsp.example.pojo.User;
import com.bosssoft.hr.train.jsp.example.service.UserService;
import com.bosssoft.hr.train.jsp.example.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.List;

/**
 * @description:
 * @author: Administrator
 * @create: 2020-05-30 11:22
 * @since
 **/
public class UpdateUserController extends HttpServlet {
    /**
     *  用户对象
     */
    private UserService userService=new UserServiceImpl();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        User user = new User();
        user.setId(Integer.parseInt(req.getParameter("id")));
        if (req.getParameter("code")!=null && !req.getParameter("code").equals("")) {
            user.setName(req.getParameter("name"));
            user.setCode(req.getParameter("code"));
            user.setPassword(req.getParameter("password"));
            try{
                if (updateUser(user)) {
                    resp.sendRedirect("queryUser");
                } else {
                    req.getRequestDispatcher("err.jsp").forward(req, resp);
                }
            } catch (Exception ex){
                req.getRequestDispatcher("queryUser").forward(req, resp);
            }
        } else {
            Query query = new Query(Integer.parseInt(req.getParameter("id")));
            List<User> userList = queryUser(query);
            user.setId(userList.get(0).getId());
            user.setName(userList.get(0).getName());
            user.setCode(userList.get(0).getCode());
            user.setPassword(userList.get(0).getPassword());
            req.setAttribute("user", user);
            req.getRequestDispatcher("update.jsp").forward(req, resp);
        }

    }

    private boolean updateUser(User user){
        return userService.update(user);
    }

    private List<User> queryUser(Query query){
        return  userService.queryByCondition(query);
    }
}
