package com.bosssoft.hr.train.jsp.example.dao.daoImpl;

import com.bosssoft.hr.train.jsp.example.dao.UserDao;
import com.bosssoft.hr.train.jsp.example.pojo.Query;
import com.bosssoft.hr.train.jsp.example.pojo.User;
import com.bosssoft.hr.train.jsp.example.util.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @description:
 * @author: Administrator
 * @create: 2020-05-30 10:42
 * @since
 **/
public class UserDaoImpl implements UserDao {
    @Override
    public int insert(User user) {
        DBUtil.init();
        if (user.getId() == null) {
            String sql = "INSERT INTO t_user (name, code, password) VALUES ('" + user.getName() + "','"
                    + user.getCode() + "','" + user.getPassword() + "')";
            int i = DBUtil.executeUpdate(sql);
            DBUtil.closeConnection();
            return i;
        } else {
            String sql1 = "SELECT * FROM t_user WHERE id=" + user.getId();
            try {
                if (DBUtil.executeQuery(sql1).next()) {
                    DBUtil.closeConnection();
                    return 0;
                } else {
                    String sql2 = "INSERT INTO t_user VALUES (" + user.getId() + ",'" + user.getName() + "','"
                            + user.getCode() + "','" + user.getPassword() + "')";
                    int i = DBUtil.executeUpdate(sql2);
                    DBUtil.closeConnection();
                    return i;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return 0;
        }
    }

    @Override
    public int deleteById(Integer id) {
        DBUtil.init();
        String sql = "DELETE FROM t_user WHERE id=" + id;
        int i = DBUtil.executeUpdate(sql);
        DBUtil.closeConnection();
        return i;
    }

    @Override
    public int update(User user) {
        DBUtil.init();
        String sql = "UPDATE t_user SET name='" + user.getName() + "',code='" + user.getCode() + "',password='"
                + user.getPassword() + "' WHERE id=" + user.getId();
        int i = DBUtil.executeUpdate(sql);
        DBUtil.closeConnection();
        return i;
    }

    @Override
    public List<User> queryByCondition(Query queryCondition) {
        DBUtil.init();
        String sql = "";
        if (queryCondition.getName() != null && !"".equals(queryCondition.getName())) {
            sql = "SELECT * FROM t_user WHERE name LIKE '%" + queryCondition.getName() + "%'";
        } else if (queryCondition.getCode() != null && queryCondition.getPassword() != null) {
            sql = "SELECT * FROM t_user WHERE code='" + queryCondition.getCode() + "' AND password='"
                    + queryCondition.getPassword() + "'";
        } else if (queryCondition.getId() != null) {
            sql = "SELECT * FROM t_user WHERE id='" + queryCondition.getId() + "'";
        } else {
            sql = "SELECT * FROM t_user";
        }
        ResultSet resultSet = DBUtil.executeQuery(sql);
        List<User> userList = new ArrayList<>();
        int flag = 0;
        try {
            while (resultSet.next()) {
                flag = 1;
                User user = new User();
                user.setId(resultSet.getInt("id"));
                user.setName(resultSet.getString("name"));
                user.setCode(resultSet.getString("code"));
                user.setPassword(resultSet.getString("password"));
                userList.add(user);
            }
        } catch (SQLException e) {
                e.printStackTrace();
        }
        if (flag == 0) {
            userList = null;
        }
        DBUtil.closeConnection();
        return userList;
    }
}
