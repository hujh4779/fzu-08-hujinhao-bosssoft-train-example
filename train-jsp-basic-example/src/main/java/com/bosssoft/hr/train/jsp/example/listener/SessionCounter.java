package com.bosssoft.hr.train.jsp.example.listener;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class SessionCounter implements HttpSessionListener {

    private int num = 0;

    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        num++;
        ServletContext context = httpSessionEvent.getSession().getServletContext();
        context.setAttribute("count", num);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        if (num > 0) {
            num--;
            ServletContext context = httpSessionEvent.getSession().getServletContext();
            context.setAttribute("count", num);
        }
    }

}
