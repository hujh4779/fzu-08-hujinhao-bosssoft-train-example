package com.bosssoft.hr.train.jsp.example.pojo;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-29 14:09
 * @since
 **/
public class Query {
    private Integer id;
    private String name;

    private String code;
    private String password;

    public Query(Integer id) {
        this.id = id;
    }
    public Query(String name) {
        this.name = name;
    }
    public Query(String code, String password) {
        this.code = code;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public String getPassword() {
        return password;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
