package com.bosssoft.hr.train.jsp.example.service.impl;

import com.bosssoft.hr.train.jsp.example.dao.daoImpl.UserDaoImpl;
import com.bosssoft.hr.train.jsp.example.exception.BusinessException;
import com.bosssoft.hr.train.jsp.example.pojo.Query;
import com.bosssoft.hr.train.jsp.example.pojo.User;
import com.bosssoft.hr.train.jsp.example.service.UserService;

import java.util.List;

/**
 * @description:
 * @author: Administrator
 * @create: 2020-05-30 10:24
 * @since
 **/

public class UserServiceImpl implements UserService {
    @Override
    public boolean save(User user) {
        try {
            // do you logic
            UserDaoImpl userDao = new UserDaoImpl();
            return userDao.insert(user) != 0;
        }catch (Exception ex){
            throw new BusinessException("10001",ex.getMessage(),ex);
        }
    }

    @Override
    public boolean remove(User user) {
        UserDaoImpl userDao = new UserDaoImpl();
        return userDao.deleteById(user.getId()) != 0;
    }

    @Override
    public boolean update(User user) {
        UserDaoImpl userDao = new UserDaoImpl();
        return userDao.update(user) != 0;
    }

    @Override
    public List<User> queryByCondition(Query queryCondition) {
        UserDaoImpl userDao = new UserDaoImpl();
        return userDao.queryByCondition(queryCondition);
    }

    @Override
    public boolean authentication(User user) {
        Query queryCondition = new Query(user.getCode(), user.getPassword());
        UserDaoImpl userDao = new UserDaoImpl();
        return userDao.queryByCondition(queryCondition) != null;
    }
}
