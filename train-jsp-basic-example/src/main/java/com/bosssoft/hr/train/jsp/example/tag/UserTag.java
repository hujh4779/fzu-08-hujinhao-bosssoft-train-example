package com.bosssoft.hr.train.jsp.example.tag;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * @description: 定义<boss:userTag /> 标签
 * @author: Administrator
 * @create: 2020-05-29 13:50
 * @since
 **/
public class UserTag extends TagSupport {

    transient PageContext userPageContext;

    @Override
    public void setPageContext(PageContext pageContext) {
        userPageContext = pageContext;
    }

    @Override
    public int doStartTag() {
        JspWriter out = userPageContext.getOut();
        HttpServletRequest request = (HttpServletRequest) userPageContext.getRequest();
        HttpSession session = request.getSession();
        String code = (String) session.getAttribute("code");
        try {
            out.write(code);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
