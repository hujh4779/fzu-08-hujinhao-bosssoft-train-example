<%@ page import="com.bosssoft.hr.train.jsp.example.pojo.User" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: HS
  Date: 2020/7/14
  Time: 20:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>增加</title>
</head>
<body>

<%
    User user = (User) request.getAttribute("user");
%>

<form action="addUser" method="post">
    昵称：<input type="text" name="name" value="<%=user!=null?user.getName():""%>" /><br />
    用户名：<input type="text" name="code" value="<%=user!=null?user.getCode():""%>" /><br />
    密码：<input type="password" name="password" value="<%=user!=null?user.getPassword():""%>" /><br />
    <input type="hidden" name="id" value="<%=user!=null?user.getId():"0"%>">
    <input type="submit" value="增加" />
    <button><a href="queryUser">取消</a></button>
</form>

</body>
</html>
