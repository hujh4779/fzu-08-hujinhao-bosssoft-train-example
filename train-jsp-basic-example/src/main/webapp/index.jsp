<%@ page import="com.bosssoft.hr.train.jsp.example.pojo.User" %>
<%@ page import="java.util.List" %>
<%@ page import="com.bosssoft.hr.train.jsp.example.listener.SessionCounter" %><%--
  Created by IntelliJ IDEA.
  User: HS
  Date: 2020/7/14
  Time: 14:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="boss" uri="UserTag" %>
<html>
<head>
    <title>首页</title>
</head>
<body>

<div class="workingArea">
    <h1 class="label label-info">用户管理</h1>

    登录账户：<boss:userTag />
    在线人数：<%=application.getAttribute("count")%>

    <br />

    <form action="queryUser" method="post">
        <input type="text" name="name" />
        <input type="submit" value="搜索" />
    </form>

    <button><a href="addUser">新增</a></button>

    <br />
    <br />
    <div class="listDataTableDiv">
        <table class="table table-striped table-bordered table-hover table-condensed">
            <thead>
            <tr class="success">
                <th>ID</th>
                <th>姓名</th>
                <th>用户名</th>
                <th>密码</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <%
                List users = (List)request.getAttribute("userList");
            %>
            <c:forEach var="user" items="<%=users%>">
            <tr>
                <td>${user.id}</td>
                <td>${user.name}</td>
                <td>${user.code}</td>
                <td>${user.password}</td>
                <td><a href="removeUser?id=${user.id}">删除</a></td>
                <td><a href="updateUser?id=${user.id}">修改</a></td>
            </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <div class="pageDiv">

    </div>
</div>

</body>
</html>