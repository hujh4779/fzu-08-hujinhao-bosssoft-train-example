<%--
  Created by IntelliJ IDEA.
  User: HS
  Date: 2020/7/14
  Time: 14:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>登录</title>
</head>

<body>

    <form action="">
        用户名:<input type="text" id="myinput" name="code" />
        <br />
        密码:<input type="password" id="mypwd" name="password" />
        <br />
        <input type="button" id="mybt" onclick="jump()" value="登录">
        <br />
        <span id="myspan"></span>
    </form>

</body>

<script type="text/javascript">

    var ajax;

    function getAjax() {
        try {
            ajax = new XMLHttpRequest();
        } catch (e) {
            ajax = new ActiveXObject("Microsoft.XMLHTTP");
        }
        return ajax;
    }

    function jump() {
        ajax = getAjax();
        ajax.open("POST", "/train_jsp_basic_example_war_exploded/login", true);
        ajax.onreadystatechange = function() {
            if (ajax.readyState === 4 && ajax.status === 200) {
                var result = ajax.responseText;
                if (result === "true") {
                    location.href = "queryUser";
                    document.getElementById("mybt").disabled = false;
                } else {
                    document.getElementById("myspan").innerHTML = "用户名或密码错误！";
                }
            }
        };
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        var code = document.getElementById("myinput").value;
        var password = document.getElementById("mypwd").value;
        var param = "code=" + code + "&password=" + password;
        ajax.send(param);
    }
</script>

</html>
